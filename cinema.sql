/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50140
Source Host           : localhost:3306
Source Database       : cinema

Target Server Type    : MYSQL
Target Server Version : 50140
File Encoding         : 65001

Date: 2014-06-30 12:59:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `permission` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Aren', 'Amirjanyan', 'amirjanyan.aren@gmail.com', '094-498-798', 'images/aren.jpg', '0');
INSERT INTO `admin` VALUES ('12', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'Aren', 'Amirjanyan', 'arencho1988@mail.ru', '095898575', 'images/aren.jpg', '1');

-- ----------------------------
-- Table structure for agenda
-- ----------------------------
DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `hour` time DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL,
  `film_length` int(4) DEFAULT NULL,
  `ticket_price` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agenda
-- ----------------------------
INSERT INTO `agenda` VALUES ('1', '2014-06-25', '10:00:00', '1', '104', '1500');
INSERT INTO `agenda` VALUES ('2', '2014-06-26', '12:00:00', '2', '95', '1700');
INSERT INTO `agenda` VALUES ('3', '2014-06-26', '14:00:00', '3', '120', '2000');
INSERT INTO `agenda` VALUES ('4', '2014-06-26', '15:00:00', '1', '104', '1500');
INSERT INTO `agenda` VALUES ('5', '2014-06-26', '17:00:00', '2', '95', '1700');
INSERT INTO `agenda` VALUES ('7', '2014-06-27', '10:00:00', '1', '104', '1500');
INSERT INTO `agenda` VALUES ('8', '2014-06-27', '12:00:00', '2', '95', '1700');
INSERT INTO `agenda` VALUES ('9', '2014-06-27', '14:00:00', '3', '120', '2000');
INSERT INTO `agenda` VALUES ('10', '2014-06-27', '15:00:00', '1', '104', '1500');
INSERT INTO `agenda` VALUES ('11', '2014-06-30', '17:00:00', '2', '95', '1700');
INSERT INTO `agenda` VALUES ('12', '2014-06-27', '19:00:00', '3', '120', '2000');
INSERT INTO `agenda` VALUES ('13', '2014-06-28', '12:00:00', '4', '85', '1700');
INSERT INTO `agenda` VALUES ('14', '2014-06-28', '12:00:00', '3', '120', '1700');
INSERT INTO `agenda` VALUES ('15', '2014-06-29', '12:00:00', '1', '104', '2000');
INSERT INTO `agenda` VALUES ('16', '2014-06-26', '19:00:00', '2', '95', '2000');
INSERT INTO `agenda` VALUES ('17', '2014-06-30', '12:00:00', '4', '85', '2500');
INSERT INTO `agenda` VALUES ('18', '2014-06-30', '12:00:00', '2', '95', '1400');
INSERT INTO `agenda` VALUES ('19', '2014-07-01', '12:00:00', '4', '85', '2000');
INSERT INTO `agenda` VALUES ('20', '2014-07-01', '12:00:00', '5', '120', '2000');

-- ----------------------------
-- Table structure for films
-- ----------------------------
DROP TABLE IF EXISTS `films`;
CREATE TABLE `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `genre` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `director` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `length` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of films
-- ----------------------------
INSERT INTO `films` VALUES ('1', 'Սպանված աղավնի', 'Ֆիլմը նկարահանվել է Նարդոսի համանուն վեպի հիման վրա:Ֆիլմում շատ են ռոմանտիկ դրվագները:', '4', '/images/1404062884_spanvac_axavni.jpg', '2010-02-22', 'Հրաչ Քեշիշյան', 'Հայաստան', '104');
INSERT INTO `films` VALUES ('2', 'Խաչագողի հիշատակարանը', 'Ֆիլմը նկարահանվել է Րաֆֆու համանուն վեպի հիման վրա:Ֆիլմում 1800-ականների դեպքեր են ներառված ու պատմում է այդ ժամանակներում տիրող անօրինականությունների մասին', '1', '/images/1404062846_xachagoxi_hishatakaran.jpg', '2011-10-01', 'Հրաչ Քեշիշյան', 'Հայաստան', '95');
INSERT INTO `films` VALUES ('3', 'Գարեգին Նժդեհ', 'Ֆիլմը իրենից ներկայացնում է Գարեգին Նժդեհի կյանքը, որտեղ ներառված են Սյունիքի, Ղարաքիլիսայի, Սարդարապատի ինքնապաշտպանությունները:Ֆիլմը նկարահանվել է իրական դեպքերի հիման վրա, որոշակի դրվագներում ֆիլմում ֆիլմի ռեժիսորը զերծ է մնացել ավելնորդություններից և այն ներկայացնում է հանրությանը ավելի փակ տեսարաններով:', '9', '/images/1404062864_garegin_njdeh.jpg', '2013-06-26', 'Հրաչ Քեշիշյան', 'Հայաստան', '120');
INSERT INTO `films` VALUES ('4', 'Քայլ Ձիով', 'Ֆիլմը շատ լավն է:Ֆիլմի շատ դրվագներ  նկարահանվել Արցախում:Այն իրենից ներկայացնում է հումորային ֆիլմ՝ հիմնված դինամիկայի և ճկունության վրա:', '2', '/images/1404117535_slug-2931.jpg', '2013-11-28', 'Հրաչ Քեշիշյան', 'Հայաստան', '85');
INSERT INTO `films` VALUES ('5', 'Կորած մոլորվածը', 'Ֆիլմը նկարահանված է ոչ իրական դեպքերի հիման վրա:Ֆիլմում նկարագրվում է, թե ինչպես է ամերիկացին օդապարիկով վայէջք կատարում Հայաստանի սահմանում, և վերջինիս մեղադրում են որպես շպիոնի:', '2', '/images/1404055673_korac_molorvac.jpg', '2013-12-10', 'Հրաչ Քեշիշյան', 'Հայաստան', '120');

-- ----------------------------
-- Table structure for genre
-- ----------------------------
DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of genre
-- ----------------------------
INSERT INTO `genre` VALUES ('1', 'Պատմական');
INSERT INTO `genre` VALUES ('2', 'Հումորային');
INSERT INTO `genre` VALUES ('3', 'Արկածային');
INSERT INTO `genre` VALUES ('4', 'Դրամատիկ');
INSERT INTO `genre` VALUES ('5', 'Դեդեկտիվ');
INSERT INTO `genre` VALUES ('6', 'Սարսափ');
INSERT INTO `genre` VALUES ('7', 'Ընտանեկան');
INSERT INTO `genre` VALUES ('8', 'Ֆանտաստիկ');
INSERT INTO `genre` VALUES ('9', 'Հայրենասիրական');
INSERT INTO `genre` VALUES ('10', 'Մարտական');
INSERT INTO `genre` VALUES ('11', 'Մշակութային');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `film_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `hour` time DEFAULT NULL,
  `row` int(2) DEFAULT NULL,
  `column` int(2) DEFAULT NULL,
  `ticket_price` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('2', '2', '3', '2014-06-26', '12:00:00', '1', '1', '1700');
INSERT INTO `orders` VALUES ('3', '2', '3', '2014-06-27', '12:00:00', '1', '2', '2000');
INSERT INTO `orders` VALUES ('4', '2', '3', '2014-06-27', '12:00:00', '2', '2', '2000');
INSERT INTO `orders` VALUES ('5', '2', '2', '2014-06-26', '19:00:00', '3', '3', '2000');
INSERT INTO `orders` VALUES ('6', '2', '2', '2014-06-26', '19:00:00', '5', '5', '2000');
INSERT INTO `orders` VALUES ('7', '2', '2', '2014-06-26', '19:00:00', '1', '4', '2000');
INSERT INTO `orders` VALUES ('8', '2', '2', '2014-06-26', '19:00:00', '7', '7', '2000');
INSERT INTO `orders` VALUES ('9', '2', '2', '2014-06-26', '19:00:00', '2', '1', '2000');
INSERT INTO `orders` VALUES ('10', '2', '2', '2014-06-26', '00:00:00', '7', '2', '2000');
INSERT INTO `orders` VALUES ('11', '2', '2', '2014-06-26', '00:00:00', '6', '7', '1700');
INSERT INTO `orders` VALUES ('12', '2', '2', '2014-06-26', '17:00:00', '1', '1', '1700');
INSERT INTO `orders` VALUES ('13', '2', '2', '2014-06-26', '17:00:00', '7', '7', '1700');
INSERT INTO `orders` VALUES ('14', '2', '2', '2014-06-26', '17:00:00', '4', '3', '1700');
INSERT INTO `orders` VALUES ('15', '2', '2', '2014-06-26', '17:00:00', '2', '5', '1700');
INSERT INTO `orders` VALUES ('16', '2', '2', '2014-06-26', '12:00:00', '7', '1', '1700');
INSERT INTO `orders` VALUES ('17', '2', '2', '2014-06-26', '19:00:00', '1', '7', '1700');
INSERT INTO `orders` VALUES ('18', '2', '2', '2014-06-26', '17:00:00', '7', '4', '1700');
INSERT INTO `orders` VALUES ('19', '2', '2', '2014-06-26', '19:00:00', '5', '2', '1700');
INSERT INTO `orders` VALUES ('20', '2', '1', '2014-06-26', '15:00:00', '1', '1', '1500');
INSERT INTO `orders` VALUES ('21', '2', '2', '2014-06-26', '19:00:00', '7', '1', '2000');
INSERT INTO `orders` VALUES ('22', '2', '4', '2014-06-30', '12:00:00', '4', '4', '2500');
INSERT INTO `orders` VALUES ('23', '2', '2', '2014-06-30', '12:00:00', '1', '7', '1400');
INSERT INTO `orders` VALUES ('24', '2', '2', '2014-06-30', '12:00:00', '4', '3', '1400');
INSERT INTO `orders` VALUES ('25', '2', '2', '2014-06-30', '12:00:00', '1', '1', '1400');
INSERT INTO `orders` VALUES ('26', '2', '2', '2014-06-30', '12:00:00', '7', '1', '1400');
INSERT INTO `orders` VALUES ('27', '2', '2', '2014-06-30', '12:00:00', '7', '7', '1400');
INSERT INTO `orders` VALUES ('28', '2', '2', '2014-06-30', '12:00:00', '3', '5', '1400');
INSERT INTO `orders` VALUES ('29', '2', '2', '2014-06-30', '12:00:00', '7', '5', '1400');
INSERT INTO `orders` VALUES ('30', '2', '4', '2014-07-01', '12:00:00', '1', '1', '2000');
INSERT INTO `orders` VALUES ('31', '2', '4', '2014-07-01', '12:00:00', '4', '2', '2000');
INSERT INTO `orders` VALUES ('32', '2', '4', '2014-07-01', '12:00:00', '7', '1', '2000');
INSERT INTO `orders` VALUES ('33', '2', '4', '2014-06-30', '12:00:00', '4', '1', '2500');
INSERT INTO `orders` VALUES ('34', '2', '4', '2014-07-01', '12:00:00', '4', '7', '2000');
INSERT INTO `orders` VALUES ('35', '2', '2', '2014-06-30', '12:00:00', '1', '4', '1400');
INSERT INTO `orders` VALUES ('36', '2', '2', '2014-06-30', '12:00:00', '7', '3', '1400');
INSERT INTO `orders` VALUES ('37', '2', '2', '2014-06-30', '12:00:00', '3', '1', '1400');
INSERT INTO `orders` VALUES ('38', '15', '2', '2014-06-30', '12:00:00', '5', '1', '1400');
INSERT INTO `orders` VALUES ('39', '2', '4', '2014-06-30', '12:00:00', '6', '6', '2500');
INSERT INTO `orders` VALUES ('40', '15', '2', '2014-06-30', '12:00:00', '1', '3', '1400');
INSERT INTO `orders` VALUES ('41', '2', '2', '2014-06-30', '12:00:00', '5', '7', '1400');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'Aren', 'Amirjanyan', 'amirjanyan.aren@gmail.com', '098-498-798', '/images/1404117179_aren.png');
INSERT INTO `users` VALUES ('15', 'user2', '7e58d63b60197ceb55a1c487989a3720', 'Aren', 'Amirjanyan', 'arencho1988@mail.ru', '095898575', '/images/1404117215_aren.png');
