<?php

class FilmsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column4';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('@'),
            ),
           array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Films;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Films'])) {
            $model->attributes = $_POST['Films'];
            $imageUploadFile = CUploadedFile::getInstance($model, 'image');
            if ($imageUploadFile !== null) { // only do if file is really uploaded
                $imageFileName = mktime() . '_' . $imageUploadFile->name;
                $model->image = '/images/' . $imageFileName;
            }
            if (Admin::model()->getPermissionAll() == false) {
                $model->save(false);
                Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
                if ($imageUploadFile !== null) // validate to save file
                    $imageUploadFile->saveAs('images/' . $imageFileName);
                $this->redirect(array('view', 'id' => $model->id));
            }
            if (Admin::model()->getPermission(Yii::app()->user->getId()) == true) {
                $model->save(false);
                if ($imageUploadFile !== null) // validate to save file
                    $imageUploadFile->saveAs('images/' . $imageFileName);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Films'])) {
            $model->attributes = $_POST['Films'];
            $imageUploadFile = CUploadedFile::getInstance($model, 'image');
            if ($imageUploadFile !== null) { // only do if file is really uploaded
                $imageFileName = mktime() . '_' . $imageUploadFile->name;
                $model->image = '/images/' . $imageFileName;
            }
            if (Admin::model()->getPermissionAll() == false) {
                $model->save(false);
                Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
                if ($imageUploadFile !== null) // validate to save file
                    $imageUploadFile->saveAs('images/' . $imageFileName);
                $this->redirect(array('view', 'id' => $model->id));
            }
            if (Admin::model()->getPermission(Yii::app()->user->getId()) == true) {
                $model->save(false);
                if ($imageUploadFile !== null) // validate to save file
                    $imageUploadFile->saveAs('images/' . $imageFileName);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Admin::model()->getPermissionAll() == false) {
            $this->loadModel($id)->delete();
            Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
        }
        if (Admin::model()->getPermission(Yii::app()->user->getId()) == true)
            $this->loadModel($id)->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Films');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Films('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Films']))
            $model->attributes = $_GET['Films'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Films the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Films::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Films $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'films-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
