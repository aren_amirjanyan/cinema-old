<?php

class OrdersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column4';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'agendahour', 'place'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Orders;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Orders'])) {
            $model->attributes = $_POST['Orders'];
            if (Admin::model()->getPermissionAll() == false) {
                $model->save(false);
                Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
                $this->redirect(array('view', 'id' => $model->id));
            }
            if (Admin::model()->getPermission(Yii::app()->user->getId()) == true) {
                $model->save(false);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Orders'])) {
            $model->attributes = $_POST['Orders'];
            if (Admin::model()->getPermissionAll() == false) {
                $model->save(false);
                Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
                $this->redirect(array('view', 'id' => $model->id));
            }
            if (Admin::model()->getPermission(Yii::app()->user->getId()) == true) {
                $model->save(false);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Admin::model()->getPermissionAll() == false) {
            $this->loadModel($id)->delete();
            Admin::model()->getChangePermissionTrue(Yii::app()->user->getId());
        }
        if (Admin::model()->getPermission(Yii::app()->user->getId()) == true)
            $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Orders');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Orders('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Orders']))
            $model->attributes = $_GET['Orders'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Orders the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Orders::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Orders $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'orders-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAgendaHour() {
        $data = Agenda::model()->findAllByAttributes(array('date' => $_POST['date'], 'film_id' => (int) $_POST['film_id']));
        $data = CHtml::listData($data, 'id', 'hour');
        $data['no'] = 'Ընտրել ժամը';
        if (!empty($data)) {
            foreach ($data as $key => $value)
                echo CHtml::tag('option', array('value' => $value), CHtml::encode($value), true);
        } else
            echo CHtml::tag('option', array('value' => 0), 'Օրակարգում չկա ֆիլմը', true);
    }

    public function actionPlace() {
        $data = Orders::model()->findAllByAttributes(array('date' => $_POST['date'], 'film_id' => (int) $_POST['film_id'], 'hour' => $_POST['hour']));
        $array = array();
        foreach ($data as $key => $value) {
            $array[$key]['row'] = $value->row;
            $array[$key]['column'] = $value->column;
        }
        $array[$key]['ticket_price'] = Agenda::model()->findByAttributes(array('date' => $_POST['date'], 'film_id' => (int) $_POST['film_id'], 'hour' => $_POST['hour']))->ticket_price;
        echo CJSON::encode($array);
        Yii::app()->end();
    }

}
