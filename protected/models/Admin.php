<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $profile_image
 */
class Admin extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'admin';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('password,login', 'required', 'message' => '{attribute}ը մուտքագրված չէ:'),
            array('login, password, first_name, last_name, email, phone, profile_image', 'length', 'max' => 255),
            array('login,email', 'unique', 'on' => 'CreateAdmin'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, login, password, first_name, last_name, email, phone, profile_image', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Հ/Հ',
            'login' => 'Ծածկանուն',
            'password' => 'Գաղտնաբառ',
            'first_name' => 'Անուն',
            'last_name' => 'Ազգանուն',
            'email' => 'էլ.հասցե',
            'phone' => 'Հեռախոս',
            'profile_image' => 'Անձնական նկար',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('profile_image', $this->profile_image, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Admin the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getPermissionAll() {
        $permission = $this->model()->findAll();
        if (!empty($permission)) {
            foreach ($permission as $key => $value) {
                if ($value->permission == 1) {
                    $boolean = true;
                    break;
                } else {
                    $boolean = false;
                }
            }
        }
        return $boolean;
    }

    public function getPermission($user) {
        $permission = $this->model()->findByAttributes(array('login' => $user));
        if ($permission->permission == 1)
            $boolean = true;
        else
            $boolean = false;
        return $boolean;
    }

    public function getChangePermissionTrue($user) {
        $changePermission = self::model()->findByAttributes(array('login' => $user));
        $changePermission->permission = 1;
        $changePermission->save();
    }

    public function getChangePermissionFalse($user) {
        $changePermission = self::model()->findByAttributes(array('login' => $user));
        $changePermission->permission = 0;
        $changePermission->save();
    }

}
