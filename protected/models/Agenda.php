<?php

/**
 * This is the model class for table "agenda".
 *
 * The followings are the available columns in table 'agenda':
 * @property integer $id
 * @property string $date
 * @property string $hour
 * @property integer $film_id
 * @property integer $film_length
 * @property integer $ticket_price
 * @property string $name
 * @property string $description
 * @property string $genre
 * @property string $image
 * @property string $film_date
 * @property string $director
 * @property string $country
 */
class Agenda extends CActiveRecord {

    public $name;
    public $description;
    public $genre;
    public $image;
    public $film_date;
    public $director;
    public $country;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'agenda';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('film_id, film_length, ticket_price', 'numerical', 'integerOnly' => true),
            array('date, hour', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, date, hour, film_id, film_length, ticket_price,name,description,film_date,director,country,genre', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'date' => 'Ամսաթիվ',
            'hour' => 'Ժամը',
            'film_id' => 'Ֆիլմը',
            'film_length' => 'Ֆիլմի տևողությունը',
            'ticket_price' => 'Ֆիլմի արժեքը',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('hour', $this->hour, true);
        $criteria->compare('film_id', $this->film_id);
        $criteria->compare('film_length', $this->film_length);
        $criteria->compare('ticket_price', $this->ticket_price);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Agenda the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getAgenda() {
        $criteria = new CDbCriteria;
        $criteria->alias = 'agenda';
        $criteria->select = 'flm.`name`,
                            flm.description,
                            flm.date as film_date,
                            flm.director,
                            flm.country,
                            flm.genre,
                            flm.image,
                            agenda.id,
                            agenda.date,
                            agenda.`hour`,
                            agenda.film_length,
                            agenda.ticket_price';
        $criteria->join = 'LEFT JOIN films flm ON flm.id=agenda.film_id';
        $criteria->order = 'agenda.date ASC';
        $criteria->condition = 'agenda.date>=CURDATE()';
        $criteria->params = array();
        return self::model()->findAll($criteria);
    }

    public function getFilmName($id) {
        $FilmName = Films::model()->findByPk($id);
        return $FilmName->name;
    }

}
