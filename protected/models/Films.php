<?php

/**
 * This is the model class for table "films".
 *
 * The followings are the available columns in table 'films':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $genre
 * @property string $image
 * @property string $date
 * @property string $director
 * @property string $country
 * @property integer $length
 */
class Films extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'films';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('length', 'numerical', 'integerOnly' => true),
            array('name, image, director, country,genre', 'length', 'max' => 255),
            array('description, date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, description, genre,image, date, director, country, length', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Հ/Հ',
            'name' => 'Ֆիլմի անվանում',
            'description' => 'Ֆիլմի նկարագրություն',
            'genre' => 'Ժանր',
            'image' => 'Ֆիլմի նկար',
            'date' => 'Ստեղծման ամսաթիվ',
            'director' => 'Ռեժիսոր',
            'country' => 'Երկիր',
            'length' => 'Տևողություն',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('genre', $this->image, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('director', $this->director, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('length', $this->length);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Films the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getFilmList() {
        return CHtml::listData($this->findAll(), 'id', 'name');
    }

}
