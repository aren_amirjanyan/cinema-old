<?php
/* @var $this AdvocateController */
/* @var $model Advocate */
/* @var $form CActiveForm */
?>

<div class="view">
    <table>
        <tr>
            <td><a href="/admins"><image src="/images/admin/administrator.png" width="110"></a><br>Ադմինիստրատոր</td>
            <td><a href="/users"><image src="/images/admin/users.png" width="110"></a><br>Հաճախորդներ</td>
            <td><a href="/films"><image src="/images/admin/films.jpg" width="120"></a><br>Ֆիլմեր</td>
        </tr>
        <tr><td><a href="/orders"><image src="/images/admin/orders.png" width="120"></a><br>Պատվերներ</td>
            <td><a href="/agenda"><image src="/images/admin/agenda.png" width="120"></a><br>Օրակարգ</td>
        </tr>
    </table>
</div><!-- form -->