<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Ադմինիստրատոր'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Ադմինիստրատորների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել ադմինիստրատոր', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admin-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'login',
		//'password',
		'first_name',
		'last_name',
		'email',
		'phone',
		'profile_image',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
