<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Ադմինիստրատորներ'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Ադմինիստրատորների ցանկ', 'url'=>array('index')),
	array('label'=>'Ադմինիստրատորների կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>