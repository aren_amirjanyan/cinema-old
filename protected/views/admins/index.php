<?php
/* @var $this AdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ադմինիստրատորներ',
);

$this->menu=array(
	array('label'=>'Ստեղծել ադմինիստրատոր', 'url'=>array('create')),
	array('label'=>'Ադմինիստրատորների կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'summaryText' => '',
)); ?>
