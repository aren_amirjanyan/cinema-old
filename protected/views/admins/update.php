<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs = array(
    'Ադմինիստրատորներ' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Փոփոխել',
);

$this->menu = array(
    array('label' => 'Ադմինիստրատորների ցանկ', 'url' => array('index')),
    array('label' => 'Ստեղծել ադմինիստրատոր', 'url' => array('create')),
    array('label' => 'Տեսնել ադմինիստրատորին', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Ադմինիստրատորների կառավարում', 'url' => array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model' => $model)); ?>