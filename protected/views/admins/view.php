<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs = array(
    'Ադմինիստրատորներ' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Ադմինիստրատորների ցանկ', 'url' => array('index')),
    array('label' => 'Ստեղծել ադմինիստրատոր', 'url' => array('create')),
    array('label' => 'Փոփոխել ադմինիստրատորին', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Ջնջել ադմինիստրատորին', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Ադմինիստրատորների կառավարում', 'url' => array('admin')),
);
?>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'login',
        'password',
        'first_name',
        'last_name',
        'email',
        'phone',
        'profile_image',
    ),
));
?>
