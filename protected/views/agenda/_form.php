<?php
/* @var $this AgendaController */
/* @var $model Agenda */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'agenda-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note"><span class="required">*</span>-ով դաշտերը պարտադիր են</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Agenda[date]',
            'value' => $model->date,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'showButtonPanel' => true,
            ),
            'htmlOptions' => array(
                'style' => ''
            ),
        ));
        ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'hour'); ?>
        <?php echo $form->textField($model, 'hour'); ?>
        <?php echo $form->error($model, 'hour'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'film_id'); ?>
        <?php
        echo $form->dropDownList($model, 'film_id', Films::model()->getFilmList()
                , array(
            'ajax' => array(
                'type' => 'POST',
                'url' => CController::createUrl('agenda/getfilmlength'),
                'success' =>
                'js:function(data){ var json = $.parseJSON(data);
                $("#Agenda_film_length").val(json);}',
        )));
        ?>
        <?php echo $form->error($model, 'film_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'film_length'); ?>
        <?php echo $form->textField($model, 'film_length',array('readonly' => 'readonly')); ?>
        <?php echo $form->error($model, 'film_length'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'ticket_price'); ?>
        <?php echo $form->textField($model, 'ticket_price'); ?>
        <?php echo $form->error($model, 'ticket_price'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Ստեղծել' : 'Պահպանել'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
