<?php
/* @var $this AgendaController */
/* @var $data Agenda */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
    <?php echo CHtml::encode($data->date); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('hour')); ?>:</b>
    <?php echo CHtml::encode($data->hour); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('film_id')); ?>:</b>
    <?php echo Agenda::model()->getFilmName($data->film_id); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('film_length')); ?>:</b>
    <?php echo CHtml::encode($data->film_length); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('ticket_price')); ?>:</b>
    <?php echo CHtml::encode($data->ticket_price); ?>
    <br />


</div>