<?php
/* @var $this AgendaController */
/* @var $model Agenda */

$this->breadcrumbs=array(
	'Օրակարգեր'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Օրակարգերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել օրակարգ', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#agenda-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'agenda-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'date',
		'hour',
		'film_id',
		'film_length',
		'ticket_price',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
