<?php
/* @var $this AgendaController */
/* @var $model Agenda */

$this->breadcrumbs=array(
	'Օրակարգեր'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Օրակարգերի ցանկ', 'url'=>array('index')),
	array('label'=>'Օրակարգի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>