<?php
/* @var $this AgendaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Օրակարգեր',
);

$this->menu=array(
	array('label'=>'Ստեղծել օրակարգ', 'url'=>array('create')),
	array('label'=>'Օրակարգի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'summaryText' => '',
)); ?>
