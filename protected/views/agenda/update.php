<?php
/* @var $this AgendaController */
/* @var $model Agenda */

$this->breadcrumbs=array(
	'Օրակարգեր'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Փոփոխել',
);

$this->menu=array(
	array('label'=>'Օրակարգի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել օրակարգ', 'url'=>array('create')),
	array('label'=>'Տեսնել օրակարգը', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Օրակարգի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>