<?php
/* @var $this AgendaController */
/* @var $model Agenda */

$this->breadcrumbs = array(
    'Օրակարգեր' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'Օրակարգի ցանկ', 'url' => array('index')),
    array('label' => 'Ստեղծել օրակարգ', 'url' => array('create')),
    array('label' => 'Փոփոխել օրակարգը', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Ջնջել օրակարգը', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Օրակարգի կառավարում', 'url' => array('admin')),
);
?>
<h1><?php echo Agenda::model()->getFilmName($model->film_id) ?></h1>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'film_id',
            'value' =>  Agenda::model()->getFilmName($model->film_id)),
        'date',
        'hour',
        'film_length',
        'ticket_price',
    ),
));
?>
<div class ='hall_by_film'>
    <div class="close">Close</div>
    <div class="my_halls">
        <h3 style="color: #ffffff">Կինոթատրոնի ազատ և զբաղված տեղերը</h3>
        <?php
        $data = Orders::model()->findAllByAttributes(array('date' => $model->date, 'film_id' => $model->film_id, 'hour' => $model->hour));
        $chair = array();
        foreach ($data as $key => $value) {
            $chair[$key] = $value->row . "_" . $value->column;
        }
        $row = 8;
        $column = 8;
        $text = '<table>';
        for ($i = 1; $i < $row; $i++) {
            $text .= '<tr>';
            for ($j = 1; $j < $column; $j++) {
                if (in_array($i . '_' . $j, $chair)) {
                    $text .= '<td><div id = "' . $i . '" data = "' . $j . '" style="background:red" class = "chair"></div></td>';
                } else {
                    $text .= '<td><div id = "' . $i . '" data = "' . $j . '" style="background:blue" class = "chair"></div></td>';
                }
            }
            $text .= '</tr>';
        }
        $text .= '</table>';
        echo $text;
        ?>
    </div>
    <table style="width: 150px;">
        <tr>
            <td><div style="background:red;width: 50px;height: 20px;"></div></td>
            <td><span>-Զբաղված</span></td>
        </tr>
        <tr>
            <td><div style="background:blue;width: 50px;height: 20px;"></div></td>
            <td><span>-Ազատ</span></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".hall_by_film").css("display", "block");
        $('.close').click(function() {
            $('.hall_by_film').css('display', 'none ');
        })

    })
</script>