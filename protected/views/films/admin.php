<?php
/* @var $this FilmsController */
/* @var $model Films */

$this->breadcrumbs=array(
	'Ֆիլմեր'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Ֆիլմերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել ֆիլմ', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#films-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'films-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'name',
		'description',
                //'genre',
		//'image',
		'date',
		'director',
		/*
		'country',
		'length',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
