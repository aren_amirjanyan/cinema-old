<?php
/* @var $this FilmsController */
/* @var $model Films */

$this->breadcrumbs=array(
	'Ֆիլմեր'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Ֆիլմերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ֆիլմերի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>