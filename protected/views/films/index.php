<?php
/* @var $this FilmsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ֆիլմեր',
);

$this->menu=array(
	array('label'=>'Ստեղծել ֆիլմ', 'url'=>array('create')),
	array('label'=>'Ֆիլմերի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'summaryText' => '',
)); ?>
