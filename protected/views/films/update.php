<?php
/* @var $this FilmsController */
/* @var $model Films */

$this->breadcrumbs=array(
	'Ֆիլմեր'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Փոփոխել',
);

$this->menu=array(
	array('label'=>'Ֆիլմերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել ֆիլմ', 'url'=>array('create')),
	array('label'=>'Տեսնել ֆիլմը', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Ֆիլմերի կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>