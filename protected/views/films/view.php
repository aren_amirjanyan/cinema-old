<?php
/* @var $this FilmsController */
/* @var $model Films */

$this->breadcrumbs = array(
    'Ֆիլմեր' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'Ֆիլմերի ցանկ', 'url' => array('index')),
    array('label' => 'Ստեղծել ֆիլմ', 'url' => array('create')),
    array('label' => 'Փոփոխել ֆիլմը', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Ջնջել ֆիլմը', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Ֆիլմերի կառավարում', 'url' => array('admin')),
);
?>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'description',
        'genre',
        'image',
        'date',
        'director',
        'country',
        'length',
    ),
));
?>
