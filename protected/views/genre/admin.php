<?php
/* @var $this GenreController */
/* @var $model Genre */

$this->breadcrumbs=array(
	'Ժանրեր'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Ժանրի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել ժանր', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#genre-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'genre-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
