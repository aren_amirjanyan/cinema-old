<?php
/* @var $this GenreController */
/* @var $model Genre */

$this->breadcrumbs=array(
	'Ժանրեր'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Ժանրերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ժանրերի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>