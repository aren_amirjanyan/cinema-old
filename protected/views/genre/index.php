<?php
/* @var $this GenreController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ժանրեր',
);

$this->menu=array(
	array('label'=>'Ստեղծել ժանր', 'url'=>array('create')),
	array('label'=>'Ժանրերի կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'summaryText' => '',
)); ?>
