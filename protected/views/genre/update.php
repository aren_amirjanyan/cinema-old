<?php
/* @var $this GenreController */
/* @var $model Genre */

$this->breadcrumbs=array(
	'Ժանրեր'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Փոփոխել',
);

$this->menu=array(
	array('label'=>'Ժանրերի ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել ժանր', 'url'=>array('create')),
	array('label'=>'Տեսնել ժանրը', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Ժանրերի կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>