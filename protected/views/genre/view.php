<?php
/* @var $this GenreController */
/* @var $model Genre */

$this->breadcrumbs = array(
    'Ժանրեր' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'Ժանրերի ցանկ', 'url' => array('index')),
    array('label' => 'Ստեղծել ժանր', 'url' => array('create')),
    array('label' => 'Փոփոխել ժանրը', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Ջնջել ժանրը', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Ժանրի կառավարում', 'url' => array('admin')),
);
?>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
    ),
));
?>
