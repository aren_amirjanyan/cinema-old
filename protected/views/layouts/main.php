<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <?php echo CHtml::image('/images/top.jpg', 'logo'); ?>
            </div><!-- header -->

            <div id="mainmenu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Գլխավոր', 'url' => array('/site/index')),
                        array('label' => 'Պատվերներ', 'url' => array('/site/myorders'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Պատվիրել տոմս', 'url' => array('/site/createorders'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Կոնտակտ', 'url' => array('/site/contact')),
                        array('label' => 'Մուտք', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Ելք (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                ));
                ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'homeLink' => 'Գլխավոր',
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <?php echo CHtml::image('/images/footer.jpg', 'logo', array('width' => 900)); ?>
                <div style="margin-left:315px;margin-top: -30px;position: absolute;z-index: 2;font-weight: bold;">
                    Ստեղծված է &copy; <?php echo date('Y'); ?> Արեն Ամիրջանյանի կողմից<br/>
                    Բոլոր իրավունքները պաշտպանված են<br/>
                </div>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
