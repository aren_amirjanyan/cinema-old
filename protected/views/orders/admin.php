<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Պատվերներ'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Պատվերների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել պատվեր', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#orders-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orders-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'user_id',
		'film_id',
		'date',
		'hour',
		'row',
		'column',
		'ticket_price',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
