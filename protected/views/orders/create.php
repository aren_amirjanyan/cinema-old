<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Պատերներ'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Պատվերների ցանկ', 'url'=>array('index')),
	array('label'=>'Պատվերների կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>