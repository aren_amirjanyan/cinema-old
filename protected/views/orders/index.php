<?php

/* @var $this OrdersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Պատվերներ',
);

$this->menu = array(
    array('label' => 'Ստեղծել պատվեր', 'url' => array('create')),
    array('label' => 'Պատվերիների կառավարում', 'url' => array('admin')),
);
?>
<?php

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
    'summaryText' => '',
));
?>
