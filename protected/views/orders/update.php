<?php
/* @var $this OrdersController */
/* @var $model Orders */

$this->breadcrumbs=array(
	'Պատվերներ'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Փոփոխել',
);

$this->menu=array(
	array('label'=>'Պատվերների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել պատվերներ', 'url'=>array('create')),
	array('label'=>'Տեսնել պատվերը', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Պատվերների կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>