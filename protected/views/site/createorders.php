<?php
/* @var $this OrdersController */
/* @var $model Orders */
/* @var $form CActiveForm */
?>
<div class="form">
    <div class ='hall'>
        <div class="close">Close</div>
        <div class="my_halls">
            <h3 style="color: #ffffff">Կինոթատրոնի ազատ և զբաղված տեղերը</h3>
        </div>
        <table style="width: 150px;">
            <tr>
                <td><div style="background:red;width: 50px;height: 20px;"></div></td>
                <td><span>-Զբաղված</span></td>
            </tr>
            <tr>
                <td><div style="background:blue;width: 50px;height: 20px;"></div></td>
                <td><span>-Ազատ</span></td>
            </tr>
        </table>

    </div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'orders-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span>-ով դաշտերը պարտադիր են</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Orders[date]',
            'value' => $model->date,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                'showButtonPanel' => true,
                'onSelect' => 'js:function(dateText, inst){$("#Orders_film_id").attr("disabled", false);}'
            ),
            'htmlOptions' => array(
                'style' => '',
                'readonly' => 'readonly',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'film_id'); ?>
        <?php
        echo $form->dropDownList($model, 'film_id', array(0 => 'Ֆիլմն ընտրված չէ', 'Ֆիլմերի ցանկ' => Films::model()->getFilmList()), array(
            'ajax' => array(
                'type' => 'POST', //request type
                'url' => CController::createUrl('orders/agendahour'),
                'update' => '#Orders_hour',
                'data' => 'js:{date: $("#Orders_date").val(),film_id:$("#Orders_film_id").val() }'
            ))
        );
        ?>
        <?php echo $form->error($model, 'film_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'hour'); ?>

        <?php
        echo $form->dropDownList($model, 'hour', array('0' => 'Ժամը ընտրված չէ', 'Ֆիլմի ժամերը' => ''), array(
            'ajax' => array(
                'type' => 'POST', //request type
                'url' => CController::createUrl('orders/place'),
                'update' => '',
                'data' => 'js:{date: $("#Orders_date").val(),film_id:$("#Orders_film_id").val(),hour:$("#Orders_hour").val() }',
                'success' =>
                'js:function(data){ 
                    $(".chair").css("background-color", "blue");
                    $(".hall").css("display","block");
                    var json = $.parseJSON(data);
                     $.each(json,function(k,v){
                     var chair=".chair_"+v.row+"_"+v.column;
                    $("#Orders_ticket_price").val(v.ticket_price);
                    $(chair).css("background-color", "red");
                    $(chair).attr("disabled", "disabled");
                    
                }); 
                }',
        )));
        ?>
        <?php echo $form->error($model, 'hour'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'row'); ?>
        <?php echo $form->textField($model, 'row', array('readonly' => 'readonly')); ?>
        <?php echo $form->error($model, 'row'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'column'); ?>
        <?php echo $form->textField($model, 'column', array('readonly' => 'readonly')); ?>
        <?php echo $form->error($model, 'column'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'ticket_price'); ?>
        <?php echo $form->textField($model, 'ticket_price', array('readonly' => 'readonly')); ?>
        <?php echo $form->error($model, 'ticket_price'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Ստեղծել' : 'Պահպանել'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
<div id="dialog" title="Dialog Title"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Orders_film_id').attr("disabled", true);
        function chair(row, column)
        {
            var text = '<table>';
            for (i = 1; i < row; i++)
            {
                text += '<tr>';
                for (j = 1; j < column; j++)
                {
                    text += '<td><div id="' + i + '" data="' + j + '" class="chair chair_' + i + '_' + j + '"></div></td>';
                }
                text += '</tr>';
            }
            text += '</table>';
            return text;
        }
        $('.my_halls').append(chair(8, 8));
        $(".chair").click(function() {
            var row = $('#Orders_row').val();
            var column = $('#Orders_column').val();
            $(".chair_" + row + "_" + column).css("background-color", "blue");
            $(this).css("background-color", 'red');
            $('#Orders_row').val($(this).attr('id'));
            $('#Orders_column').val($(this).attr('data'));
        });
        $('.close').click(function() {
            $('.hall').css('display', 'none');
        })

    })
</script><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

