<div class="titles">
    <h3>Ցուցադրվող ֆիլմերի ցանկ</h3>
</div>
<?php
$films = Agenda::model()->getAgenda();
$time = 0;
foreach ($films as $film) {
    if ($time < strtotime($film->date)) {
        echo '<br><div class="date">
<h3>' . date("d-m-Y", strtotime($film->date)) . '</h3>
    </div>';
    }
    $time = strtotime($film->date);
    echo '<div class="film_list">
        <table>
       <tr>
       <td>' . CHtml::image($film->image, 'image', array('width' => 150)) . '</td>
       <td><h2>'.CHtml::image('/images/film_title.png', 'image', array('width' => 50)).'<span class="film_description">' . $film->name . '</span></h2>' . $film->description . '</td>
       </tr>
       <tr>
       <td>Սկիզբը</td>
       <td><span class="right">Ժամը՝' . $film->hour . '-ին</span></td>
       </tr>
       <tr>
       <td>Ժանր</td>
       <td><span class="right">' . Genre::model()->findByPk($film->genre)->name . '</span></td>
       </tr>
       <tr>
       <td>Ֆիլմի տևողությունը</td>
       <td><span class="right">' . $film->film_length . ' րոպե</span></td>
       </tr>
       <tr>
       <td>Ֆիլմի ռեժիսորը</td>
       <td><span class="right">' . $film->director . '</span></td>
       </tr>
       <tr>
       <td>Տոմսի արժեքը</td>
       <td><span class="right">' . $film->ticket_price . ' դրամ</span></td>
       </tr>
       </table></div>';
}
?>