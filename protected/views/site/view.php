<?php
/* @var $this OrdersController */
/* @var $model Orders */

?>

<h1>Տոմս N<?php
    if ($model->row == 1)
        $row_rd = '-ին';
    else
        $row_rd = '-րդ';
    if ($model->column == 1)
        $column_rd = '-ին';
    else
        $column_rd = '-րդ';
    echo $model->id;
    echo '<br>' . $model->row . $row_rd . ' շարքի ' . $model->column . $column_rd . ' տեղի համար` գինը ' . $model->ticket_price . ' դրամ:'; {
        
    };
    ?></h1>
    <?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        array('name' => 'film_id',
            'value' => Agenda::model()->getFilmName($model->film_id)),
        'date',
        'hour',
    ),
));
?>
<div class ='hall_by_film'>
    <div class="close">Close</div>
    <div class="my_halls">
        <h3 style="color: #ffffff">Կինոթատրոնում զբաղեցրած տեղը</h3>
        <?php
        $chair = array($model->row . "_" . $model->column);
        $row = 8;
        $column = 8;
        $text = '<table>';
        for ($i = 1; $i < $row; $i++) {
            $text .= '<tr>';
            for ($j = 1; $j < $column; $j++) {
                if (in_array($i . '_' . $j, $chair)) {
                    $text .= '<td><div id = "' . $i . '" data = "' . $j . '" style="background:red" class = "chair"></div></td>';
                } else {
                    $text .= '<td><div id = "' . $i . '" data = "' . $j . '" style="background:blue" class = "chair"></div></td>';
                }
            }
            $text .= '</tr>';
        }
        $text .= '</table>';
        echo $text;
        ?>
    </div>
    <table style="width: 150px;">
        <tr>
            <td><div style="background:red;width: 50px;height: 20px;"></div></td>
            <td><span>-Զբաղված</span></td>
        </tr>
        <tr>
            <td><div style="background:blue;width: 50px;height: 20px;"></div></td>
            <td><span>-Ազատ</span></td>
        </tr>
    </table>
</div>
