<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Հաճախորդներ'=>array('index'),
	'Կառավարում',
);

$this->menu=array(
	array('label'=>'Հաճախորդների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել հաճախորդ', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'summaryText' => '',
	'columns'=>array(
		'id',
		'login',
		'password',
		'first_name',
		'last_name',
		'email',
		/*
		'phone',
		'profile_image',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
