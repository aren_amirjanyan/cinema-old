<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Հաճախորդներ'=>array('index'),
	'Ստեղծել',
);

$this->menu=array(
	array('label'=>'Հաճախորդների ցանկ', 'url'=>array('index')),
	array('label'=>'Հաճախորդների կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>