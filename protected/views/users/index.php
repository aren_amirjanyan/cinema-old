<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Հաճախորդներ',
);

$this->menu=array(
	array('label'=>'Ստեղծել հաճախորդ', 'url'=>array('create')),
	array('label'=>'Հաճախորդների կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'summaryText' => '',
)); ?>
