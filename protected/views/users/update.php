<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Հաճախորդներ'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Փոփոխել',
);

$this->menu=array(
	array('label'=>'Հաճախորդների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել հաճախորդ', 'url'=>array('create')),
	array('label'=>'Տեսնել հաճախորդին', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Հաճախորդների կառավարում', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>