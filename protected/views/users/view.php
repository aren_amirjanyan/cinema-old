<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Հաճախորդներ'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Հաճախորդների ցանկ', 'url'=>array('index')),
	array('label'=>'Ստեղծել հաճախորդ', 'url'=>array('create')),
	array('label'=>'Փոփոխել հաճախորդին', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Ջնջել հաճախորդին', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Հաճախորդների կառավարում', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'login',
		'password',
		'first_name',
		'last_name',
		'email',
		'phone',
		'profile_image',
	),
)); ?>
